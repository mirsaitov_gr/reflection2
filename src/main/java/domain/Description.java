package domain;

import java.lang.reflect.Field;

public class Description {

    private String name;

    private Field field;

    public Description(String name, Field field) {
        this.name = name;
        this.field = field;
    }

    public String getName() {
        return name;
    }

    public Field getField() {
        return field;
    }

}

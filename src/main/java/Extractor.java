import domain.Description;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public class Extractor {

    public static void upload(List<Object> objects, String fileName) {
        if (objects.isEmpty()) {
            return;
        }

        //Проверка, что объекты одного класса и получить описание
        Class<?> type = objects.get(0).getClass();
        for (Object object : objects) {
            if (!Objects.equals(type, object.getClass())) {
                throw new IllegalArgumentException("Distinct class");
            }
        }
        List<Description> descriptions = new LinkedList<>();
        while (type != null) {
            descriptions.addAll(getDescriptionsFromClass(type));
            type = type.getSuperclass();
        }

        upload(descriptions, objects, fileName);
    }

    private static List<Description> getDescriptionsFromClass(Class<?> clazz) {
        List<Description> descriptions = new LinkedList<>();
        for (Field field : clazz.getDeclaredFields()) {
            Class<?> type = field.getClass();
            descriptions.add(new Description(field.getName(), field));
        }
        return descriptions;
    }

    private static void upload(List<Description> descriptions, List<Object> objects, String fileName) {
        try (PrintWriter pw = new PrintWriter(new File(fileName))) {
            //Записать заголовок
            List<String> headers = descriptions.stream().map(Description::getName).collect(Collectors.toList());
            pw.println(convertToCSVLine(headers));
            objects.forEach(object -> pw.println(convertToCSVLine(getDataFromObject(descriptions, object))));
        } catch (FileNotFoundException exception) {
            System.out.println(exception.getMessage());
        }
    }

    private static List<String> getDataFromObject(List<Description> descriptions, Object object) {
        List<String> fields = new LinkedList<>();
        descriptions.forEach(description -> {
            Field field = description.getField();
            boolean canAccess = field.canAccess(object);
            try {
                Object value;
                if (!canAccess) {
                    field.setAccessible(true);
                    value = field.get(object);
                    field.setAccessible(false);
                } else {
                    value = field.get(object);
                }
                fields.add(Objects.toString(value));
            } catch (IllegalAccessException exception) {
                fields.add("error");
                System.out.println(exception.getMessage());
            }
        });
        return  fields;
    }

    private static String convertToCSVLine(List<String> data) {
        return data.stream().collect(Collectors.joining(","));
    }

}

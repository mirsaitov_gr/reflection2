import domain.Person;
import domain.Test2;

import java.time.LocalDate;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Object> persons = List.of(new Person()
                , new Person("TEST", "TEST", LocalDate.now(), "TEST")
                , new Person("TEST", null, null, "TEST")
        );

        Extractor.upload(persons, "test.csv");

        List<Object> tests = List.of(new Test2("test", "test")
                , new Test2("test1", "test1")
        );
        Extractor.upload(tests, "test1.csv");
    }

}
